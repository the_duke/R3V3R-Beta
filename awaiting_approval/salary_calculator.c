/**
 * @file salary_calculator.c
 * @brief Asks user for salary information and calculates the total salary.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 26.03.2018
 *
 */

#include <stdio.h>

#define BONUS 15.0

int main()
{
	float hours = 0;
	float salary = 0;
	float total = 0;

	printf("How many hours did you work: ");
	scanf("%f", &hours);

	printf("What is your salary: ");
	scanf("%f", &salary);

	total = hours * salary;

	printf("Your total salary: %0.2f\n", total);
	printf("Your bonus: %0.2f\n", BONUS);
	printf("Updated salary: %0.2f\n", total + BONUS);

	return 0;
}
