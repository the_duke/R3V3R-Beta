/**
 * @file number_exchange.c
 * @brief Asks user for two numbers, assigns them into variables and switches the variables.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, ?.?.2018
 *
 */

#include <stdio.h>

int main()
{
	int num1 = 0;
	int num2 = 0;
	int temp = 0;

	printf("Please enter two numbers to exchange: ");
	scanf("%d %d", &num1, &num2);

	printf("Before exchange:\n");
	printf("Num1 - %d\n", num1);
	printf("Num2 - %d\n", num2);

	temp = num1;
	num1 = num2;
	num2 = temp;

	printf("After exchange:\n");
	printf("Num1 - %d\n", num1);
	printf("Num2 - %d\n", num2);

	return 0;
}

