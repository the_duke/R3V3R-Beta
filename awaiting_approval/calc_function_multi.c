/**
 * @file calc_function_multi.c
 * @brief Added multi calculating function to the basic calculator
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 27.03.2018
 *
 */

#include <stdio.h>

int multi_calculate(int* num1, int* num2, int* num3);

int main()
{	
	int num1 = 0;
	int num2 = 0;
	int num3 = 0;
	int result = 0;

	printf("Please enter two numbers to show their addition and difference: ");
	scanf("%d %d", &num1, &num2);

	printf("Sum: %d\n", num1 + num2);
	printf("Difference: %d\n", num1 - num2);

	printf("Now enter three numbers to activate the magic function: ");
	scanf("%d %d %d", &num1, &num2, &num3);

	result = multi_calculate(&num1, &num2, &num3);
	
	printf("First number: %d\n", num1);
	printf("Second number: %d\n", num2);
	printf("Third number: %d\n", num3);
	printf("Return value: %d\n", result);

	return 0;
}

/**
 * Each passed variable will contain the sum of it and it's next variable
 *
 * @param num1 - first number
 * @param num2 - second number
 * @param num3 - third number
 *
 * @return int - The sum of the three passed variables
 */
int multi_calculate(int* num1, int* num2, int* num3)
{
	int temp = 0;
	int sum = 0;

	sum = *num1 + *num2 + *num3;
	
	// Because num3 is using the changed num1(which we don't want) we have to store the value of num1 before assigning it
	temp = *num1;

	*num1 = *num1 + *num2;
	*num2 = *num2 + *num3;
	*num3 = *num3 + temp;

	return sum;
}
