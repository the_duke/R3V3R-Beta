/**
 * @file bij_printer.c
 * @brief Gets a number from user and prints the numbers from 0 to it. Ends when user enters a number lower than 1.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 27.03.2018
 *
 */

#include <stdio.h>

int main()
{
	int num = 0;
	int i = 0;

	printf("Enter numbers to print the numbers from 1 to it: (Enter 0 or lower to end)\n");
	scanf("%d", &num);

	while(num > 0) {
		for(i = 1; i <= num; i++) {
			printf("%d ", i);
		}

		printf("\n");

		scanf("%d", &num);
	}

	return 0;
}

