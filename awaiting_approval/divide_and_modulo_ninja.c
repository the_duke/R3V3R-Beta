/**
 * @file divide_and_modulo.c
 * @brief Asks user for two numbers and prints their division and modulo.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 27.03.2018
 *
 */

#include <stdio.h>

int divide(int num1, int num2);
int modulo(int num1, int num2);

int main()
{
	int num1 = 0;
	int num2 = 0;

	printf("Please enter two numbers to show their division and modulo: ");
	scanf("%d %d", &num1, &num2);

	printf("Division: %d\n", divide(num1, num2));
	printf("Modulo: %d\n", modulo(num1, num2));

	return 0;
}

/**
 * Performs a division on two numbers
 *
 * @param num1 - The divided number
 * @param num2 - The divisor
 *
 * @return int - The result of the division
 */
int divide(int num1, int num2)
{
	if(num1 < num2) {
		return 0;
	}

	return divide(num1 - num2, num2) + 1;
}

/**
 * Performs a modulo operation on two numbers
 *
 * @param num1 - The divided number
 * @param num2 - The divisor
 *
 * @return int - The remainder of the division
 */
int modulo(int num1, int num2)
{
	if(num1 < num2) {
		return num1;
	}

	return modulo(num1 - num2, num2);
}
