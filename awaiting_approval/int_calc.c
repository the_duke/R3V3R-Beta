/**
 * @file int_calc.c
 * @brief Gets two integers from the user and prints their sum and difference.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 26.03.2018
 *
 */

#include <stdio.h>

int main()
{
	int num1 = 0;
	int num2 = 0;

	printf("Please enter two numbers to show their addition and difference: ");
	scanf("%d %d", &num1, &num2);

	printf("Sum: %d\n", num1 + num2);
	printf("Difference: %d\n", num1 - num2);

	return 0;
}

