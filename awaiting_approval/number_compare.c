/**
 * @file number_compare.c
 * @brief Gets two number from the user and prints the bigger one or if they are equal.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 28.03.2018
 *
 */

#include <stdio.h>

int main()
{
	int num1 = 0;
	int num2 = 0;

	printf("Please enter two number to compare: ");
	scanf("%d %d", &num1, &num2);

	if(num1 > num2) {
		printf("%d is bigger than %d\n", num1, num2);
	} else if (num2 > num1) {
		printf("%d is bigger than %d\n", num2, num1); 
	} else {
		printf("Both numbers are equal.\n");
	}

	return 0;
}

