/**
 * @file array_printer.c
 * @brief Prints the values in an array to the screen.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 28.03.2018
 *
 */

#include <stdio.h>

#define ARRAY_LEN 5

int main()
{
	int arr[ARRAY_LEN] = {1, 2, 3, 4, 5};
	int i = 0;

	for(i = 0; i < ARRAY_LEN; i++) {
		printf("%d ", arr[i]);
	}
	printf("\n");
	
	return 0;
}

