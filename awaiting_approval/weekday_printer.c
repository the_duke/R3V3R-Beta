/**
 * @file weekday_printer.c
 * @brief Gets weekday number from user and prints the name of the weekday accordingly.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 27.03.2018
 *
 */

#include <stdio.h>

enum WEEKDAY_NAMES {
	SUNDAY = 1,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY
};

int main()
{
	int day_number = 0;

	printf("Please enter the day of the week: ");
	scanf("%d", &day_number);

	if(day_number >= SUNDAY && day_number <= FRIDAY) {
		printf("You have chosen ");

		switch(day_number) {
		case SUNDAY: 
			printf("Sunday");
			break;
		case MONDAY:
			printf("Monday");
			break;
		case TUESDAY:
			printf("Tuesday");
			break;
		case WEDNESDAY:
			printf("Wednesday");
			break;
		case THURSDAY:
			printf("Thursday");
			break;
		case FRIDAY:
			printf("Friday");
			break;
		case SATURDAY:
			printf("Saturday");
			break;
		default:
			break;
		}

		printf("\n");
	}

	return 0;
}

