/**
 * @file int_caster.c
 * @brief Gets an integer from the user, casts it to different data types and prints it back to the user.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 26.03.2018
 *
 */

#include <stdio.h>

int main()
{
	int integer = 0;
	char character = 0;
	unsigned int u_integer = 0;
	float floating = 0;

	printf("Enter a number to cast it: ");
	scanf("%d", &integer);

	character = (char)integer;
	u_integer = (unsigned int)integer;
	floating = (float)integer;

	printf("char = %c\n", character);
	printf("unsigned int = %u\n", u_integer);
	printf("float = %f\n", floating);

	return 0;
}

