/**
 * @file monthname_printer.c
 * @brief Gets month number from user and prints the name of the month.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 28.03.2018
 *
 */

#include <stdio.h>

enum MONTH_NAMES {
	JANUARY = 1,
	FEBRUARY,
	MARCH,
	APRIL,
	MAY,
	JUNE,
	JULY,
	AUGUST,
	SEPTEMBER,
	OCTOBER,
	NOVEMBER,
	DECEMBER
};

int main()
{
	int month_number = 0;

	printf("Please enter the number of the month: ");
	scanf("%d", &month_number);

	if(month_number >= JANUARY && month_number <= DECEMBER) {
		printf("You have chosen ");

		switch(month_number) {
		case JANUARY: 
			printf("January");
			break;
		case FEBRUARY:
			printf("February");
			break;
		case MARCH:
			printf("March");
			break;
		case APRIL:
			printf("April");
			break;
		case MAY:
			printf("May");
			break;
		case JUNE:
			printf("June");
			break;
		case JULY:
			printf("July");
			break;
		case AUGUST:
			printf("August");
			break;
		case SEPTEMBER:
			printf("September");
			break;
		case OCTOBER:
			printf("October");
			break;
		case NOVEMBER:
			printf("November");
			break;
		case DECEMBER:
			printf("December");
			break;
		default:
			break;
		}

		printf("\n");
	}

	return 0;
}

