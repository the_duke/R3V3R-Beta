/**
 * @file is_even.c
 * @brief Gets a number from the user and prints wether it is even or not.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 27.03.2018
 *
 */

#include <stdio.h>

int main()
{
	int num = 0;

	printf("Please enter a number for me to tell if it is ever or not: ");
	scanf("%d", &num);

	if(num % 2 == 0) {
		printf("%d is even\n", num);
	} else {
		printf("%d is not even\n", num);
	}

	return 0;
}

