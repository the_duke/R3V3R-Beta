/**
 * @file float_calc.c
 * @brief Gets two float numbers from the user and prints their addition and difference.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 27.03.2018
 *
 */

#include <stdio.h>

int main()
{
	float num1 = 0;
	float num2 = 0;

	printf("Please enter two floating point numbers to show their addition and difference: ");
	scanf("%f %f", &num1, &num2);

	printf("Addition: %.2f\n", num1 + num2);
	printf("Difference: %.2f\n", num1 - num2);
	
	return 0;
}

