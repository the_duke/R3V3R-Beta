/**
 * @file divide_and_modulo.c
 * @brief Asks user for two numbers and prints their division and modulo.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 27.03.2018
 *
 */

#include <stdio.h>

int main()
{
	int num1 = 0;
	int num2 = 0;

	printf("Please enter two numbers to show their division and modulo: ");
	scanf("%d %d", &num1, &num2);

	printf("Division: %d\n", num1 / num2);
	printf("Modulo: %d\n", num1 % num2);

	return 0;
}

