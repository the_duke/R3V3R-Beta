/**
 * @file printodd.c
 * @brief Prints the odd numbers in a defined range
 *
 * 
 *
 * version log
 * ------------
 * version 1.1 by R3v3rsedReality, 25.03.2018
 * version 1.0 by R3v3rsedReality, 25.03.2018
 *
 */

#include <stdio.h>

#define FROM 1
#define TO 254

int main()
{
	int i = 0;

	for(i = FROM; i < TO; i++) {
		if(i % 2 == 1) {
			printf("%d ", i);
		}
	}

	printf("\n");
	
	return 0;
}
