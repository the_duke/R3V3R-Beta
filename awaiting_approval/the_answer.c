/**
 * @file the_answer.c
 * @brief Gets user input until 42 is entered.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 27.03.2018
 *
 */

#include <stdio.h>

#define THE_ANSWER 42

int main()
{
	int inputted_num = 0;

	printf("Please enter the answer to the ultimate question of life, the universe and everything: ");
	scanf("%d", &inputted_num);

	while(inputted_num != THE_ANSWER) {
		scanf("%d", &inputted_num);
	}

	printf("Correct!\n");

	return 0;
}

