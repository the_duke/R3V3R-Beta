/**
 * @file negative_finder.c
 * @brief Asks the user for a number and prints 1 if it's positive, -1 if it's negative and 0 it zero.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 27.03.2018
 *
 */

#include <stdio.h>

#define POSITIVE 1
#define NEGATIVE -1

int main()
{
	int num = 0;

	printf("Please enter a number: ");
	scanf("%d", &num);

	if(num < 0) {
		num = NEGATIVE;
	} else if(num > 0) {
		num = POSITIVE;
	}

	printf("%d\n", num);

	return 0;
}

