/**
 * @file <name>.c
 * @brief <description>
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 27.03.2018
 *
 */

#include <stdio.h>

int main()
{
	int num = 0;

	printf("Please enter a number to show its absolute value: ");
	scanf("%d", &num);

	if (num < 0) {
		num *= -1;
	}

	printf("%d\n", num);

	return 0;
}

