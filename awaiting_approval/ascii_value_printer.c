/**
 * @file ascii_value_printer.c
 * @brief Gets three characters from the user and prints their ascii value.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 26.03.2018
 *
 */

#include <stdio.h>

int main()
{
	char first_character = 0;
	char second_character = 0;
	char third_character = 0;

	printf("Enter three letter to show their ascii values: ");
	scanf("%c %c %c", &first_character, &second_character, &third_character);

	printf("First letter = %d\n", first_character);
	printf("Second letter = %d\n", second_character);
	printf("Third letter = %d\n", third_character);

	return 0;
}

