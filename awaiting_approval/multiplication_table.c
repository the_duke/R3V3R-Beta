/**
 * @file multiplication_table.c
 * @brief Prints the multiplication table
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, ?.?.2018
 *
 */

#include <stdio.h>

#define FROM 1
#define TO 10

int main()
{
	int i = 0;
	int j = 0;

	for(i = FROM; i <= TO; i++) {
		for(j = FROM; j <= TO; j++) {
			printf("%-4d", i * j);
		}
		printf("\n");
	}

	return 0;
}

