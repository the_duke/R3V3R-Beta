/**
 * @file print_reversed.c
 * @brief Gets an integer from the user and prints the number in it's reversed form.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 27.03.2018
 *
 */

#include <stdio.h>

void print_reversed(int num);

int main()
{
	int num = 0;

	printf("Please enter a number to print it in it's reversed order: ");
	scanf("%d", &num);

	printf("Reversed: ");
	print_reversed(num);
	printf("\n");

	return 0;
}

/**
 * Prints a number in it's reversed order.
 *
 * @param num - The number to print
 *
 * @return void
 */
void print_reversed(int num)
{
	if(num / 10 == 0) {
		printf("%d",num);
	} else {
		printf("%d",num % 10);
		print_reversed(num / 10);
	}

	return;
}
