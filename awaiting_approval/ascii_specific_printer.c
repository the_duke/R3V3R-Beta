/**
 * @file ascii_specific_printer.c
 * @brief Asks the user for a letter and prints the ascii value of it. 
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 26.03.2018
 *
 */

#include <stdio.h>

int main()
{
	char character = 0;

	printf("Please enter a letter: ");
	character = getchar();

	if(character >= 'A' && character <= 'Z') {
		printf("The ascii value of %c is %d\n", character, character);
	} else if(character >= 'a' && character <= 'a') {
		printf("The ascii value of %c is %d\n", character, character);
	} else {
		printf("Error: Inputted character is not a letter\n");
	}

	return 0;
}

