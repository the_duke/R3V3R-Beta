/**
 * @file average_calc.c
 * @brief Gets three numbers from user and prints their average.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 27.03.2018
 *
 */

#include <stdio.h>
#include <math.h>

#define NUMBER_COUNT 3.0

int main()
{
	int num1 = 0;
	int num2 = 0;
	int num3 = 0;

	printf("Enter three numbers to calculate their average: ");
	scanf("%d %d %d", &num1, &num2, &num3);

	printf("Average: %.2f\n", (num1 + num2 + num3) / NUMBER_COUNT);

	return 0;
}

