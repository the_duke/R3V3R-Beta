/**
 * @file f2c.c
 * @brief Asks the user for celcius degrees and converts it to fahrenheit.
 *
 * ++ approved by ninjailbreak, 23.03.2018
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 22.03.2018
 *
 */
#include <stdio.h>

#define KELVIN_ADD 273.15

int main()
{
	float celcius = 0;
	float kelvin = 0;

	printf("Please enter celsius degrees to convert to kelvin: ");
	scanf("%f", &celcius);

	kelvin = celcius + KELVIN_ADD;
	printf("%.1f celcius is %.1f kelvin\n", celcius, kelvin);
	
	return 0;
}

