/**
 * @file ageprinter.c
 * @brief Asks the user for his age and prints it back to the user.
 *
 * 
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 22.3.2018
 *
 */
#include <stdio.h>

int main()
{
	int age = 0;

	printf("Please enter your age: ");
	scanf("%d", &age);

	printf("\n\nYou are &d years old!", age);

	return 0;
}
