/**
 * @file nameandage.c
 * @brief Asks the user for age and first letter of his name and prints it back.
 * 
 * 
 *
 * version log
 * ------------
 * version 1.0.0 by R3v3rsedReality, 14.2.2018
 *
 */
#include <stdio.h>

int main()
{
	int age = 0;
	char first_letter = 0;

	printf("Please enter your age and your name's first letter: ");
	scanf("%d %c", &age, &first_letter);
	getchar();

	printf("Your first letter is %c and you are %d years old!\n", first_letter, age);
	return 0;
}
