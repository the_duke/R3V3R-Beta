/**
 * @file f2c.c
 * @brief Asks the user for celcius degrees and converts it to fahrenheit.
 *
 * ++ approved by ninjailbreak, 23.03.2018
 *
 * version log
 * ------------
 * version 1.0 by R3v3rsedReality, 22.03.2018
 *
 */
#include <stdio.h>

#define CELCIUS_ADD 32.0
#define CELCIUS_MULTIPLIER 1.8

int main()
{
	float celcius = 0;
	float fahrenheit = 0;
	
	printf("Please enter a number to convert from celsius to fahrenheit: ");
	scanf("%f", &celcius);

	fahrenheit = celcius * CELCIUS_MULTIPLIER + CELCIUS_ADD;

	printf("%.1f celsius is %.1f fahrenheit.\n", celcius, fahrenheit);

	return 0;
}

