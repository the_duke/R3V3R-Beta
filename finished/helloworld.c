/**
 * @file helloworld.c
 * @brief Prints "Hello Worlds" to the user
 *
 * 
 *
 * version log
 * ------------
 * version 1.0.0 by R3v3rsedReality, 14.2.2018
 *
 */
#include <stdio.h>

int main()
{
	printf("Hello World!");

	return 0;
}
