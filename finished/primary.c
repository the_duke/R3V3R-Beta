/**
 * @file primary.c
 * @brief Asks the user for a number and prints the primary number until that number
 * 
 * 
 *
 * version log
 * ------------
 * version 1.0.0 by R3v3rsedReality, 14.2.2018
 *
 */
#include <stdio.h>
#include <math.h>

#define FALSE 0
#define TRUE !FALSE

int main()
{
	int i = 0;
	int j = 0;
	char is_prime = FALSE;
	int print_until = 0;

	printf("Calculate prime numbers until? ");
	scanf("%d", &print_until);
	getchar(); // Clearing buffer;

	for(i = 1; i < print_until; i++) {
		is_prime = TRUE;
		for(j = 2; j < i && is_prime; j++) {
			if(i % j == 0) {
				is_prime = FALSE;
			}
		}

		if(is_prime) {
			printf("%d ", i);
		}
	}

	return 0;
}
